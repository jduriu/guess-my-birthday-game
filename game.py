from random import randint

# randint gives a random integer between a range
# number_between_1_and_100 = rantint(1, 100)

def guess_my_birthday():

  user_name = input("Please enter your name: ")
  attempt = 1

  while attempt <= 5:
    guess_month = randint(1, 12)
    guess_year = randint(1924, 2004)

    print(f"Guess {attempt}: {user_name} were you born in {guess_month} / {guess_year} ?")

    response = input("Yes or no? ").lower()

    if response == "yes":
      print("I knew it!")
      break
    elif response == "no" and attempt < 5:
      print("Drat! Lemme try again!")
    elif response == "no":
      print("I have other things to do. Good bye.")
    else:
      print("Answer not valid, please write 'yes' or 'no' on next try")


    attempt += 1

guess_my_birthday()
